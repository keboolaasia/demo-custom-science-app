import pip
pip.main(['install', 'gender-guesser'])
import sys
from keboola import docker
import traceback
import gender_guesser.detector as gender
import pandas as pd

PATH_ORDERS = '/data/in/tables/demo_orders.csv'
PATH_CUSTOMERS = '/data/in/tables/demo_customers.csv'
PATH_SUMMARY_OUT = '/data/out/tables/orders_summary.csv'


def parse_config(config_dir='/data/'):
    """https://github.com/keboola/python-docker-application"""
    conf = docker.Config(config_dir)
    return conf.get_parameters()

def summarize_orders(path_orders, path_customers, augment_genders):
    d = gender.Detector()

    orders = pd.read_csv(path_orders)
    customers = pd.read_csv(path_customers)

    orders['total_cost'] = orders['amount'] * orders['cost_per_piece']

    cust_orders = customers.merge(orders, on='customer_id', how='left')
    orders_summary = cust_orders.groupby('name').agg({'total_cost':sum})
    if augment_genders:
        orders_summary['gender'] = orders_summary.index.map(d.get_gender)
    
    return orders_summary


def main():
    """
    For more info, see docs
    https://developers.keboola.com/extend/custom-science/python/
    """
    params = parse_config()
    summary = summarize_orders(PATH_ORDERS, PATH_CUSTOMERS, augment_genders=params.get('augment_genders', False))
    summary.to_csv(PATH_SUMMARY_OUT)
        
if __name__ == '__main__':
    try:
        main()
    except ValueError as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    except Exception as err:
        print(err, file=sys.stderr)
        traceback.print_exc(file=sys.stderr)
        sys.exit(2)